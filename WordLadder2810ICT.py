# Import Regular Expression
import re

debug = False


# A function for debugging
def debugmsg(msg):
    if debug:
        print(msg)


# Checks how many characters match from a item(word) and a target(word) and returns the number of matches
def same(aword, targetword):
    # Return the number of matches found between characters in the item and target strings
    return len([c for (c, t) in zip(aword, targetword) if c == t])


# Build a list of potential words out of the input list based on an input pattern and a list of seen words
def build(pattern, usablewords, seenwordslist, potentialwordlist):
    # debug msg
    debugmsg("pattern= " + pattern)
    # Return A list of words found in input usablewords that match the input pattern and are not found in
    # the input dict seenwordslist and input list potentialwordlist which have already been added
    return [word for word in usablewords
            if re.search(pattern, word) and word not in seenwordslist.keys() and
            word not in potentialwordlist]


# Compile a Path List for words based on how many characters in an input word and a target word match
def find(initialword, usablewordslist, seenwordslist, targetword, pathlist):
    # Initialize an empty list
    potentialwordlist = []

    # debug msg
    debugmsg("[" + initialword + "]")

    # Loop through a range built from the length of the input word
    for i in range(len(initialword)):
        # compare each letter to make sure it dosnt already match the corresponding letter in the target word
        if not initialword[i] == targetword[i]:
            # add the return value from the build function
            # Input is the pattern with one letter changed to a .
            # as well as a lists for the usable words, seen words, and the current path of words
            # as well as a current and target word
            # this creates a list of words with only one letter difference from the current word
            potentialwordlist += build(initialword[:i] + "." + initialword[i + 1:], usablewordslist, seenwordslist,
                                       potentialwordlist)

    # If the List of potential words is empty return false as no paths were found
    if len(potentialwordlist) == 0:
        return False

    # sort list based on match value retrieved from same function
    potentialwordlist = sorted([(same(w, targetword), w) for w in potentialwordlist], reverse=True)

    # debug msg
    debugmsg("potentialwordlist= " + str(potentialwordlist))

    # Loop through all sorted item and match tuples contained in the now sorted list to find words with
    # only one letter difference(or exactly the same) as the target word while adding all other items to
    # the seen list (this checks in the program has already reached the target word)
    for (match, item) in potentialwordlist:
        # If the number of matches for the current item is greater then the length of words used -1
        if match >= len(targetword) - 1:
            # If the number of matches for the current item is exactly the same as the length of
            # words used - 1
            if match == len(targetword) - 1:
                # Then append that item to the word path as its only got one letter difference from the target word
                # debug msg
                debugmsg("word added to path= " + str(item))
                pathlist.append(item)
            # return true if the number of matches is at least word length - 1 as this means the program has reached
            # the target word
            return True
        # Set the seen value in the seen dict to True for current item(word)
        seenwordslist[item] = True

    # Recursive function loop, this keeps going until all words in the list have been tried or the target word is one
    # step off
    for (match, item) in potentialwordlist:
        # Add item(word) to path list
        # debug msg
        debugmsg("-word added to path= " + str(item))
        pathlist.append(item)
        # check whether the added item(word) has any off by one letter words connected to it
        if find(item, usablewordslist, seenwordslist, targetword, pathlist):
            # if so then return true as the path has reached the target word
            return True
        # debug msg
        debugmsg("word removed from path= " + str(item))
        # if not then remove the current item(word) from path list, this causes the function regression to
        # move onto the next potential match
        pathlist.pop()


# Get a list of words function
def inputwordlist(charlength):
    print("Enter a blank line to finish.")
    wordlist = []
    while True:
        word = input("Please enter a " + str(charlength) + " character word with only letters: ")
        if word == '':
            break
        if not len(word) == charlength:
            print("Please enter a word with " + str(charlength) + " characters.")
            continue
        if not re.match(r'\D[a-z]+\Z', word):
            print("Please enter a single word with only letters.")
            continue
        wordlist.append(word)

    return wordlist


# Get a y/n response
def inputyn():
    while True:
        response = input("Please enter a 'y' for yes, or a 'n' for no: ")
        if not re.match(r'[yn]\Z', response):
            print(response)
            print("Please enter either a y or a n")
            continue

        if response == 'y':
            return True
        else:
            return False


# Start of main program loop
if __name__ == "__main__":
    # Get basic input for File name
    filename = input("Enter dictionary name: ")
    # Open the file (dictionary)
    file = open(filename)
    # Read all file lines into a list
    lines = file.readlines()

    # Infinite input control loop
    while True:
        # Get basic input for start word
        startword = input("Enter start word:")
        # make sure user input is a single word of only letters
        if not re.match(r'\D[a-z]+\Z', startword):
            print("Please enter a single word with only letters.")
            continue

        # Initialize an empty list for usable words
        usablewords = []

        # Initialize an empty list for excluded words
        excludedwords = []

        # Get basic input for the target/end word
        targetword = input("Enter target word:")
        # make sure target word has same length as start word
        if not len(startword) == len(targetword):
            print("Target word must have the same number of characters as the start word")
            continue
        # make sure user input is a single word of only letters
        if not re.match(r'\D[a-z]+\Z', targetword):
            print("Please enter a single word with only letters.")
            continue

        # Ask for any excluded words
        print("\nWould you like to input a list of words to exclude from the ladder?")
        if inputyn():
            excludedwords = inputwordlist(len(startword))

        # Ask for any required words
        print("\nWould you like to input a list of words to include in the ladder?")
        if inputyn():
            requiredwords = inputwordlist(len(startword))
            requiredwords.append(targetword)
            excludedwords.extend(requiredwords)
        else:
            requiredwords = [targetword]

        # Loop through every line in the lines list(retrieved from file)
        for line in lines:
            # Strip the line of any white space to retrieve a word
            aword = line.rstrip()

            # Make sure its the same length as the start word
            if len(aword) == len(startword):
                # If so then append it to the potential words list
                usablewords.append(aword)

        # If excluded word list is not empty
        if len(excludedwords):
            # then removed the excluded words from the usable word list
            usablewords = [w for w in usablewords if w not in excludedwords]

        # Break the infinite input control loop
        break

    # Start the chain/ladder rung count at 0
    count = 0
    # Initialize new list (the word path list) starting with the start word
    path = [startword]
    # Initialize new dict (seen words dict) starting with the start word being seen
    seenwords = {startword: True}

    for word in requiredwords:
        # Start the recursive function find to start the pathing algorithm
        if find(startword, usablewords, seenwords, word, path):
            # Once the find function finds a path to a word that is only one letter off the target word, the target word
            # must be appended to the path list
            path.append(word)
            startword = word
        else:
            # if a path is not found then the find function never returns true and the no path found msg is printed
            print("No path found.")

    # print the length of the path - 1
    print(len(path) - 1, path)
