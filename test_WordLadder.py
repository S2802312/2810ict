# Import UnitTest
import unittest
from WordLadder2810ICT import *

class TestSame(unittest.TestCase):

    def test_zero_same(self):
        result = same("leaf","gold")
        self.assertEqual(result, 0)

    def test_one_same(self):
        result = same("lead","gold")
        self.assertEqual(result, 1)

    def test_two_same(self):
        result = same("load","gold")
        self.assertEqual(result, 2)

    def test_three_same(self):
        result = same("goad","gold")
        self.assertEqual(result, 3)

    def test_all_same(self):
        result = same("gold","gold")
        self.assertEqual(result, 4)

class TestBuild(unittest.TestCase):

    # 0 matched words, 0 words matched in seen, 0 words matched in list
    def test_zero_matches(self):
        pattern = ".ead"
        words = ["gall", "cats", "dogs", "word"]
        seen = {"puck": True}
        list = ["duck", "luck"]
        result = build(pattern, words, seen, list)
        self.assertEqual(result, [])

    # 1 matched words, 0 words matched in seen, 0 words matched in list

    # 2 matched words, 0 words matched in seen, 0 words matched in list

    # 2 matched words, 1 words matched in seen, 0 words matched in list

    # 2 matched words, 2 words matched in seen, 1 words matched in list

    # 3 matched words, 2 words matched in seen, 2 words matched in list


class TestFind(unittest.TestCase):

    # Helper function to retrieve word list
    def getwordlist(self):
        words = []
        # Open the file (dictionary)
        file = open("dictionary.txt")
        # Read all file lines into a list
        lines = file.readlines()

        # Loop through every line in the lines list(retrieved from file)
        for line in lines:
            # Strip the line of any white space to retrieve a word
            word = line.rstrip()

            # Make sure its the same length as the start word
            if len(word) == len(start):
                # If so then append it to the potential words list
                words.append(word)
        return words

    # Path Found, find returned true
    def test_path_found(self):
        initialword = "lead"
        usablewordslist = ["load", "goad"]
        seenwordslist = {"lead": True}
        targetword = "gold"
        pathlist = ["lead"]
        result = find(initialword, usablewordslist, seenwordslist, targetword, pathlist)
        self.assertTrue(result)

    # Path not found, find returned False
    def test_path_not_found(self):
        initialword = "lead"
        usablewordslist = ["load", "golf"]
        seenwordslist = {"lead": True}
        targetword = "gold"
        pathlist = ["lead"]
        result = find(initialword, usablewordslist, seenwordslist, targetword, pathlist)
        self.assertFalse(result)

    # Path Found, find returned true, Lead to Gold in 3 steps, with full word list

    # Path Found, find returned true, Hide to seek in 6 steps, with full word list

if __name__ == '__main__':
    unittest.main()